// importing libraries needed
const express = require("express");
const app = express();
const bodyParser = require("body-parser");
const port = 3000;

// importing json file using local modules
const users = require("./data/users.json");

// Using middleware to load ejs and static files
app.set("view engine", "ejs");
app.use(express.static("public"));

// Parsing the incoming data from /login routes
app.use(express.json());
app.use(express.urlencoded({ extended: true }));

// the middleware if user using undefine endpoint
const logged = function (req, res, next) {
  res.status(404);
  res.send("<h1>404: Page file not found<h1>");
  next();
};

// get the homepage
app.get("/", (req, res) => {
  res.render("home");
});

// get the login page
app.get("/login", (req, res) => {
  res.render("login");
});

// posting the input form and matching it to users.json
app.post("/login", (req, res) => {
  const { username, password } = req.body;

  const user = users.find((user) => {
    return user.username === username;
  });

  if (user) {
    const passwordMatched = user.password === password;

    if (passwordMatched) {
      // the output if username and password match
      res.render("home-user", {
        nama: user.nama,
      });
    } else {
      // the output if client input the wrong password
      console.log("wrong password");
      res.status(401).send("<h1>wrong password<h1>");
    }
  } else {
    // the output if client input undefined username
    console.log("user not found");
    res.status(404).send("<h1>User not found<h1>");
  }
});

app.get("/game", (req, res) => {
  res.render("game");
});

app.use(logged);

app.listen(port, () => {
  console.log(`App is listening on port ${port}...`);
});
