class Game {
  // generate computer choice
  getComputerChoice() {
    const comp = Math.random();
    if (comp < 0.34) return "rock";
    if (comp >= 0.34 && comp < 0.67) return "paper";
    return "scissor";
  }

  // rules method
  getResult(player, comp) {
    if (player == comp) return "draw";
    if (player == "rock") return comp == "scissor" ? "win" : "lose";
    if (player == "paper") return comp == "rock" ? "win" : "lose";
    if (player == "scissor") return comp == "paper" ? "win" : "lose";
  }

  gameStart() {
    const pRock = document.getElementById("p-rock");
    const pPaper = document.getElementById("p-paper");
    const pScissor = document.getElementById("p-scissor");

    pRock.addEventListener("click", (event) => {
      event.target.style.backgroundColor = "darkgrey";
      const playerChoice = "rock";
      const computerChoice = this.getComputerChoice();
      const result = this.getResult(playerChoice, computerChoice);

      //to get result from console
      console.log(`player: ${playerChoice}`);
      console.log(`comp: ${computerChoice}`);
      console.log(`result: ${result}`);

      //to display computer choice on UI
      if (computerChoice == "rock") {
        return (document.getElementById("c-rock").style.background = "darkgrey");
      } else if (computerChoice == "paper") {
        return (document.getElementById("c-paper").style.background = "darkgrey");
      } else if (computerChoice == "scissor") {
        return (document.getElementById("c-scissor").style.background = "darkgrey");
      }
    });

    pPaper.addEventListener("click", (event) => {
      event.target.style.backgroundColor = "darkgrey";
      const playerChoice = "paper";
      const computerChoice = this.getComputerChoice();
      const result = this.getResult(playerChoice, computerChoice);

      //to get result from console
      console.log(`player: ${playerChoice}`);
      console.log(`comp: ${computerChoice}`);
      console.log(`result: ${result}`);

      //to display computer choice on UI
      if (computerChoice == "rock") {
        return (document.getElementById("c-rock").style.background = "darkgrey");
      } else if (computerChoice == "paper") {
        return (document.getElementById("c-paper").style.background = "darkgrey");
      } else if (computerChoice == "scissor") {
        return (document.getElementById("c-scissor").style.background = "darkgrey");
      }
    });

    pScissor.addEventListener("click", (event) => {
      event.target.style.backgroundColor = "darkgrey";
      const playerChoice = "scissor";
      const computerChoice = this.getComputerChoice();
      const result = this.getResult(playerChoice, computerChoice);

      // to get result from console
      console.log(`player: ${playerChoice}`);
      console.log(`comp: ${computerChoice}`);
      console.log(`result: ${result}`);

      // to display computer choice on UI
      if (computerChoice == "rock") {
        return (document.getElementById("c-rock").style.background = "darkgrey");
      } else if (computerChoice == "paper") {
        return (document.getElementById("c-paper").style.background = "darkgrey");
      } else if (computerChoice == "scissor") {
        return (document.getElementById("c-scissor").style.background = "darkgrey");
      }
    });
  }
}

const main = new Game();

main.gameStart();
